#include "Camera.h"
#include "Input.h"


Camera::Camera(const Math3D::Vec3& position, const Math3D::Vec3& rotation):
	position_(position),
	rotation_(rotation)
{
}

void Camera::update(float dt)
{
	if (Input::Mouse::isPressed(Input::Mouse::MIDDLE))
	{
		rotation_.x += Input::Mouse::getMovement().y / 200.0f;
		rotation_.y += Input::Mouse::getMovement().x / 200.0f;
	}

	Math3D::Vec3 movement;
	if (Input::Keyboard::isPressed('Z'))
		position_.y += 0.5f * dt;
	if (Input::Keyboard::isPressed('X'))
		position_.y -= 0.5f * dt;
	if (Input::Keyboard::isPressed('A'))
		movement.x -= 1.0f;
	else if (Input::Keyboard::isPressed('D'))
		movement.x += 1.0f;
	if (Input::Keyboard::isPressed('W'))
		movement.z += 1.0f;
	else if (Input::Keyboard::isPressed('S'))
		movement.z -= 1.0f;
	movement.normalize();
	auto x  = movement.z*sinf(rotation_.y) + movement.x*cosf(rotation_.y);
	auto z = movement.x * -sinf(rotation_.y) + movement.z * cosf(rotation_.y);
	position_ += Math3D::Vec3(x,0.0f, z) * 0.5f * dt;
}

Math3D::Matrix4f Camera::getViewMatrix() const
{
	return Math3D::Matrix4f::translation(-position_.x, -position_.y, -position_.z)* Math3D::Matrix4f::rotationY(rotation_.y) * Math3D::Matrix4f::rotationX(rotation_.x) * Math3D::Matrix4f::rotationZ(rotation_.z);
}

Math3D::Vec3 Camera::getPosition()
{
	return position_;
}

Math3D::Vec3 Camera::getRotation()
{
	return rotation_;
}
