#pragma once
#include "Vectors.h"
#include "Matrix.h"

class Camera
{
public:
	Camera(const Math3D::Vec3& position = Math3D::Vec3(), const Math3D::Vec3& rotation = Math3D::Vec3());
	void update(float dt);
	Math3D::Matrix4f getViewMatrix() const;
	Math3D::Vec3 getPosition();
	Math3D::Vec3 getRotation();
	~Camera() = default;
private:
	Math3D::Vec3 position_;
	Math3D::Vec3 rotation_;
};

