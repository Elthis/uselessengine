#pragma once
#include <cstdint>

/**
 * \brief Class holding color parameters
 */
class Color {
public:
	Color(const uint8_t r = 0, const uint8_t g = 0, const uint8_t b = 0) : b(b), g(g), r(r) {}
	Color operator+(const Color& rhs) const
	{
		return Color(r + rhs.r, g + rhs.g, b + rhs.b);
	}
	Color operator-(const Color& rhs) const
	{
		return Color(r - rhs.r, g - rhs.g, b - rhs.b);
	}
	Color operator*(const float scalar) const
	{
		return Color(r*scalar, g*scalar, b*scalar);
	}
	Color operator/(const float divider) const
	{
		return Color(r * divider, g * divider, b * divider);
	}
	
	~Color() = default;
	uint8_t b;
	uint8_t g;
	uint8_t r;
};
