#include "Game.h"
#include "Matrix.h"


using Math3D::Matrix4f;

Game::Game(HWND hwnd, const size_t width, const size_t height) :
	gfx_(hwnd, width, height),
	texturedCube_(1.0f, L"./Textures/crate.bmp"),
	grassCube_(1.0f, L"./Textures/sky.bmp"),
	pipeline_(gfx_),
	projectionMatrix_(Matrix4f::projectionPerspectiveFovLH(60.0f /180.0f * 3.14f, static_cast<float>(width)/height, 0.1f, 1000.0f)),
	model_(L"./Models/porche.model", L"./Textures/porche.bmp")
{
}


void Game::loop(const float dt)
{
	static auto scale = 0.0f;
	camera_.update(dt);
	scale += 2.0f* dt;
	pipeline_.setVertexShader(&vertexShader_);
	pipeline_.setPixelShader(&pixelShader_);
	gfx_.clearScreen();
	auto viewMatrix = camera_.getViewMatrix();
	vertexShader_.setViewProjectionMatrix(viewMatrix*projectionMatrix_);
	pixelShader_.setDiffuseLightDirection(Math3D::Vec3(0.0f, 0.0f, 1.0f));
	pixelShader_.setAmbientLight(0.15f);



	auto rot = Matrix4f::rotationY(scale / 24.0f) * Matrix4f::translation(0.0f, 0.0f, 4.5f);
	vertexShader_.setTransformationMatrix(rot);
	pixelShader_.setTexture(model_.getSurface());
	pipeline_.drawTriangle(model_.getVertices());


	pixelShader_.setTexture(&texturedCube_.getSurface());
	rot =  Matrix4f::translation(2.0f, 0.0f, 6.5f);
	vertexShader_.setTransformationMatrix(rot);
	pipeline_.drawTriangle(texturedCube_.getVertices());
	gfx_.swapBuffer();

}
