#pragma once
#include "UselessEngine.h"
#include "Pipeline.h"
#include "TexturedCube.h"
#include "SimpleVertexShader.h"
#include "TexturePixelShader.h"
#include "Camera.h"
#include "Model.h"

class Game
{
public:
	Game(HWND hwnd, size_t width, size_t height);
	~Game() = default;
	void loop(float dt);
private:
	UselessEngine gfx_;
	TexturedCube texturedCube_;
	TexturedCube grassCube_;
	Pipeline<InputVertex> pipeline_;
	SimpleVertexShader<InputVertex> vertexShader_;
	TexturePixelShader<InputVertex> pixelShader_;
	Camera camera_;
	Math3D::Matrix4f projectionMatrix_;
	Model model_;
};

