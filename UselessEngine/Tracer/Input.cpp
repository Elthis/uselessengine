#include "Input.h"

bool Input::Keyboard::keys_[256] = { false };
bool Input::Mouse::buttons_[3] = { false };
Input::Mouse::Coordinates2D Input::Mouse::position_;
Input::Mouse::Coordinates2D Input::Mouse::movement_;
Concurrency::concurrent_queue<Input::Mouse::Coordinates2D> Input::Mouse::movementQueue_;


bool Input::Keyboard::isPressed(const unsigned char key)
{
	return keys_[key];
}

void Input::Keyboard::pressKey(const unsigned char key)
{
	keys_[key] = true;
}

void Input::Keyboard::releaseKey(const unsigned char key)
{
	keys_[key] = false;
}

void Input::Mouse::pressButton(const Button button)
{
	buttons_[button] = true;
}

void Input::Mouse::releaseButton(const Button button)
{
	buttons_[button] = false;
}

bool Input::Mouse::isPressed(const Button button)
{
	return buttons_[button];
}

Input::Mouse::Coordinates2D Input::Mouse::getPosition()
{
	return position_;
}

Input::Mouse::Coordinates2D Input::Mouse::getMovement()
{
	return movement_;
}


void Input::Mouse::setPosition(const int posX, const int posY)
{
	pushMovement(posX, posY);
	position_ = { posX, posY };
}

void Input::Mouse::pushMovement(const int posX, const int posY)
{
	
	movementQueue_.push({ posX - position_.x , posY - position_.y });
}

void Input::Mouse::popMovement()
{
	Coordinates2D move{};
	movement_ = { 0, 0 };
	while (movementQueue_.try_pop(move))
	{
		movement_.x += move.x;
		movement_.y += move.y;
	}
		
}

