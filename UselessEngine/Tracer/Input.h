#pragma once
#include <concurrent_queue.h>

namespace Input
{
	class Keyboard
	{
		friend class Window;
	public:
		Keyboard() = delete;
		~Keyboard() = delete;
		static bool isPressed(unsigned char key);
	private:
		static void pressKey(unsigned char key);
		static void releaseKey(unsigned char key);
		static bool keys_[256];
	};

	class Mouse
	{
		struct Coordinates2D
		{
			int x;
			int y;
		};
		friend class Window;
	public:
		enum Button
		{
			LEFT,
			RIGHT,
			MIDDLE
		};
		Mouse() = delete;
		~Mouse() = delete;
		static bool isPressed(Button button);
		static Coordinates2D getPosition();
		static Coordinates2D getMovement();
	private:
		static void setPosition(int posX, int posY);
		static void pushMovement(int posX, int posY);
		static void popMovement();
		static void pressButton(Button button);
		static void releaseButton(Button button);
		static bool buttons_[3];
		static Coordinates2D position_;
		static Coordinates2D movement_;

		
		static Concurrency::concurrent_queue<Coordinates2D> movementQueue_;


	};
	
};


