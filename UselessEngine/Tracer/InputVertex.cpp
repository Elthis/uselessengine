#include "InputVertex.h"

using namespace Math3D;

InputVertex::InputVertex(const Vec4& pos, const Vec4& uv, const Vec3& normal):
pos(pos),
uv(uv),
normal(normal)
{
}

InputVertex& InputVertex::interpolateTo(const float alpha, const InputVertex& ver)
{
	pos.interpolate(alpha, ver.pos);
	uv.interpolate(alpha, ver.uv);
	normal.interpolate(alpha, ver.normal);
	return *this;
}

InputVertex InputVertex::getInterpolated(const float alpha, const InputVertex& ver) const
{
	return InputVertex(*this).interpolateTo(alpha, ver);
}

InputVertex InputVertex::operator+(const InputVertex& vertex) const
{
	return InputVertex(pos+vertex.pos, uv+vertex.uv, normal+ vertex.normal);
}

InputVertex InputVertex::operator-(const InputVertex& vertex) const
{
	return InputVertex(pos - vertex.pos, uv - vertex.uv, normal - vertex.normal);
}

InputVertex InputVertex::operator*(const float scalar) const
{
	return InputVertex(pos * scalar, uv * scalar, normal * scalar);
}

InputVertex InputVertex::operator/(const float divider) const
{
	return InputVertex(pos / divider, uv / divider, normal /divider);
}

InputVertex& InputVertex::operator+=(const InputVertex& vertex)
{
	pos += vertex.pos;
	uv += vertex.uv;
	normal += vertex.normal;
	return *this;
}

InputVertex& InputVertex::operator-=(const InputVertex& vertex)
{
	pos -= vertex.pos;
	uv -= vertex.uv;
	normal -= vertex.normal;
	return *this;
}

InputVertex& InputVertex::operator*=(const float scalar)
{
	pos *= scalar;
	uv *= scalar;
	normal *= scalar;
	return *this;
}

InputVertex& InputVertex::operator/=(const float divider)
{
	pos /= divider;
	uv /= divider;
	normal /= divider;
	return *this;
}

InputVertex operator*(const Matrix4f& mat, const InputVertex& vertex)
{
	return InputVertex((mat * vertex.pos), vertex.uv, vertex.normal);
}

InputVertex operator*(const InputVertex& vertex, const Matrix4f& mat)
{
	return InputVertex((mat * vertex.pos), vertex.uv, vertex.normal);

}





