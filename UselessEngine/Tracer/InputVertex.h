#pragma once
#include "Math3D.h"

class InputVertex
{

public:
	InputVertex(const Math3D::Vec4& pos, const Math3D::Vec4& uv, const Math3D::Vec3& normal);
	~InputVertex() = default;
	InputVertex& interpolateTo(float alpha, const InputVertex& ver);
	InputVertex getInterpolated(float alpha, const InputVertex& ver) const;

	InputVertex operator+(const InputVertex& vertex) const;
	InputVertex operator-(const InputVertex& vertex) const;
	InputVertex operator*(float scalar) const;
	InputVertex operator/(float divider) const;
	InputVertex& operator+=(const InputVertex& vertex);
	InputVertex& operator-=(const InputVertex& vertex);
	InputVertex& operator*=(float scalar);
	InputVertex& operator/=(float divider);
	Math3D::Vec4 pos;
	Math3D::Vec4 uv;
	Math3D::Vec3 normal;
};

InputVertex operator*(const Math3D::Matrix4f& mat, const InputVertex& vertex);
InputVertex operator*(const InputVertex& vertex, const Math3D::Matrix4f& mat);