#pragma once
#include <algorithm>
#include "Vectors.h"

namespace Math3D
{
	/**
	 * \brief Square matrix template for different sizes
	 * \tparam S dimensions of square matrix (3 or 4)
	 */
	template<size_t S>
	class _Matrix
	{
	public:
		_Matrix& operator=(const _Matrix& rhs)
		{
			memcpy(m, rhs.m, sizeof(float)*S * S);
			return *this;
		}

		_Matrix& operator*=(const _Matrix& rhs)
		{
			return *this = *this * rhs;
		}

		_Matrix& operator*=(float rhs)
		{
			for (auto& row : m)
				for (auto& e : row)
					e *= rhs;
			return *this;
		}

		_Matrix operator*(const _Matrix& rhs)
		{
			_Matrix result;
			for (auto i = 0; i < S; i++) {

				for (auto j = 0; j < S; j++)
				{
					float sum = 0.0f;
					for (auto k = 0; k < S; k++)
					{
						sum += m[i][k] * rhs.m[k][j];
					}
					result.m[i][j] = sum;
				}

			}
			return result;
		}

		static constexpr _Matrix translation(float x, float y, float z)
		{
			if constexpr (S == 4)
			{
				return{
				1.0f, 0.0f, 0.0f, 0.0f,
				0.0f, 1.0f, 0.0f, 0.0f,
				0.0f, 0.0f, 1.0f, 0.0f,
				x,    y,    z,    1.0f,
				};
			}
			else
			{
				static_assert(false, "Wrong dimensions");
			}

		}

		static constexpr _Matrix scale(float x, float y, float z)
		{
			if constexpr (S == 3)
			{
				return{
					 x,  0.0f, 0.0f,
					0.0f,  y,  0.0f,
					0.0f, 0.0f,  z
				};
			}
			else if constexpr (S == 4)
			{
				return{
					 x,  0.0f, 0.0f,  0.0f,
					0.0f,  y,  0.0f,  0.0f,
					0.0f, 0.0f,  z,   0.0f,
					0.0f, 0.0f, 0.0f, 1.0f
				};
			}
			else
			{
				static_assert(false, "Wrong dimensions");
			}

		}

		/**
		 * \brief Create RotationX Matrix
		 * \param radians Angle in radians
		 * \return Matrix rotating around X axis by specified angle in radians
		 */
		static constexpr _Matrix rotationX(float radians)
		{
			const float sinValue = sin(radians);
			const float cosValue = cos(radians);
			if constexpr (S == 3)
			{
				return{
					1.0f,  0.0f,      0.0f,
					0.0f,  cosValue, -sinValue,
					0.0f,  sinValue,  cosValue
				};
			}
			else if constexpr (S == 4)
			{
				return{
					1.0f,  0.0f,       0.0f,      0.0f,
					0.0f,  cosValue, -sinValue,   0.0f,
					0.0f,  sinValue,  cosValue,   0.0f,
					0.0f,  0.0f,       0.0f,      1.0f
				};
			}
			else
			{
				static_assert(false, "Wrong dimensions");
			}

		}

		/**
		 * \brief Create RotationY Matrix
		 * \param radians Angle in radians
		 * \return Matrix rotating around Y axis by specified angle in radians
		 */
		static constexpr _Matrix rotationY(float radians)
		{
			const float sinValue = sin(radians);
			const float cosValue = cos(radians);
			if constexpr (S == 3)
			{
				return{
					 cosValue,  0.0f,  sinValue,
					 0.0f,      1.0f,  0.0f,
					-sinValue,  0.0f,  cosValue
				};
			}
			else if constexpr (S == 4)
			{
				return{
					 cosValue,  0.0f,  sinValue, 0.0f,
					 0.0f,      1.0f,  0.0f,     0.0f,
					-sinValue,  0.0f,  cosValue, 0.0f,
					 0.0f,      0.0f,  0.0f,     1.0f
				};
			}
			else
			{
				static_assert(false, "Wrong dimensions");
			}

		}

		/**
		 * \brief Create RotationZ Matrix
		 * \param radians Angle in radians
		 * \return Matrix rotating around Z axis by specified angle in radians
		 */
		static constexpr _Matrix rotationZ(float radians)
		{
			const float sinValue = sin(radians);
			const float cosValue = cos(radians);
			if constexpr (S == 3)
			{
				return{
					cosValue, -sinValue, 0.0f,
					sinValue,  cosValue, 0.0f,
					0.0f,       0.0f,    1.0f,
				};
			}
			else if constexpr (S == 4)
			{
				return{
					cosValue, -sinValue, 0.0f,  0.0f,
					sinValue,  cosValue, 0.0f,  0.0f,
					0.0f,      0.0f,     1.0f,  0.0f,
					0.0f,      0.0f,     0.0f,  1.0f,
				};
			}
			else
			{
				static_assert(false, "Wrong dimensions");
			}

		}
		/**
		 * \brief Create Identity Matrix
		 * \return Matrix with ones on diagonal
		 */
		static constexpr  _Matrix identity()
		{
			if constexpr (S == 3)
			{
				return{
					1.0f, 0.0f, 0.0f,
					0.0f, 1.0f, 0.0f,
					0.0f, 0.0f, 1.0f,
				};
			}
			else if constexpr (S == 4)
			{
				return{
					1.0f, 0.0f, 0.0f, 0.0f,
					0.0f, 1.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 1.0f, 0.0f,
					0.0f, 0.0f, 0.0f, 1.0f
				};
			}
			else
			{
				static_assert(false, "Wrong dimensions");
			}
		}

		/**
		 * \brief Create perspective projection matrix for left-handed system
		 * \param width Width of the view volume at the near view-plane.
		 * \param height Height of the view volume at the near view-plane.
		 * \param n Z-value of the near view-plane.
		 * \param f Z-value of the far view-plane.
		 * \return Matrix rotating around X axis by specified angle in radians
		 */
		static constexpr _Matrix projectionPerspectiveLH(const float width, const float height, const float n, const float f)
		{
			
			if constexpr(S==4)
			{
				const float zDiff = f - n;
				return{
					(2*n)/width, 0.0f,          0.0f,         0.0f,
					0.0f,         (2*n)/height, 0.0f,         0.0f,
					0.0f,         0.0f,         f/zDiff,      1.0f,
					0.0f,         0.0f,         (-n*f)/zDiff, 0.0f
				};
			}
			else
			{
				static_assert(false, "Wrong dimensions");
			}
		}

		/**
		 * \brief Create perspective projection matrix for left-handed coordinate system
		 * \param fov Field of view in radians
		 * \param ar Aspect ratio of view-plane.
		 * \param n Z-value of the near view-plane.
		 * \param f Z-value of the far view-plane.
		 * \return Matrix rotating around X axis by specified angle in radians
		 */
		static constexpr _Matrix projectionPerspectiveFovLH(const float fov, const float ar, const float n, const float f)
		{
			if constexpr (S == 4)
			{
				const float zDiff = f - n;
				const float w = 1.0f / std::tan(fov / 2.0f);
				const float h = w * ar;
				return{
					w,            0.0f,      0.0f,             0.0f,
					0.0f,         h,         0.0f,             0.0f,
					0.0f,         0.0f,      f / zDiff,        1.0f,
					0.0f,         0.0f,      (-n * f) / zDiff, 0.0f
				};
			}
			else
			{
				static_assert(false, "Wrong dimensions");
			}
		}

		float m[S][S];
		
	};
	typedef __declspec(align(16))_Matrix<3> Matrix3f;
	typedef __declspec(align(16))_Matrix<4> Matrix4f;
	inline Vec3 operator*(const Vec3& vec, const Matrix3f& mat)
	{
		return{
			mat.m[0][0] * vec.x + mat.m[1][0] * vec.y + mat.m[2][0]* vec.z,
			mat.m[0][1] * vec.x + mat.m[1][1] * vec.y + mat.m[2][1] * vec.z,
			mat.m[0][2] * vec.x + mat.m[1][2] * vec.y + mat.m[2][2] * vec.z,
		};
	}

	inline Vec3 operator*(const Matrix3f& mat, const Vec3& vec)
	{
		return{
			mat.m[0][0] * vec.x + mat.m[1][0] * vec.y + mat.m[2][0] * vec.z,
			mat.m[0][1] * vec.x + mat.m[1][1] * vec.y + mat.m[2][1] * vec.z,
			mat.m[0][2] * vec.x + mat.m[1][2] * vec.y + mat.m[2][2] * vec.z,
		};
	}

	inline Vec4 operator*(const Vec4& vec, const Matrix4f& mat)
	{
		return{
			mat.m[0][0] * vec.x + mat.m[1][0] * vec.y + mat.m[2][0] * vec.z + mat.m[3][0] * vec.w,
			mat.m[0][1] * vec.x + mat.m[1][1] * vec.y + mat.m[2][1] * vec.z + mat.m[3][1] * vec.w,
			mat.m[0][2] * vec.x + mat.m[1][2] * vec.y + mat.m[2][2] * vec.z + mat.m[3][2] * vec.w,
			mat.m[0][3] * vec.x + mat.m[1][3] * vec.y + mat.m[2][3] * vec.z + mat.m[3][3] * vec.w,
		};
	}

	inline Vec4 operator*(const Matrix4f& mat, const Vec4& vec)
	{
		return{
			mat.m[0][0] * vec.x + mat.m[1][0] * vec.y + mat.m[2][0] * vec.z + mat.m[3][0] * vec.w,
			mat.m[0][1] * vec.x + mat.m[1][1] * vec.y + mat.m[2][1] * vec.z + mat.m[3][1] * vec.w,
			mat.m[0][2] * vec.x + mat.m[1][2] * vec.y + mat.m[2][2] * vec.z + mat.m[3][2] * vec.w,
			mat.m[0][3] * vec.x + mat.m[1][3] * vec.y + mat.m[2][3] * vec.z + mat.m[3][3] * vec.w,
		};
	}
}
