#include "Model.h"
#include <fstream>




Model::Model(const std::wstring& model, const std::wstring& texture) :
	surface_(texture)
{
	std::wifstream file(model);
	auto vertices = 0;
	file >> vertices;
	float posX, posY, posZ, uvX, uvY, normX, normY, normZ;
	for(auto i =0 ; i < vertices; i++)
	{
		
		file >> posX >> posY >> posZ >> uvX >> uvY >> normX >> normY >> normZ;
		vertices_.emplace_back(Math3D::Vec3(posX, posY, posZ), Math3D::Vec2(uvX, uvY), Math3D::Vec3(normX, normY, normZ));
	}
	file.close();

}



std::vector<InputVertex> Model::getVertices() const
{
	return vertices_;
}

const Surface* Model::getSurface() const
{
	return &surface_;
}
