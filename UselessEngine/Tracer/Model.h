#pragma once
#include <string>
#include "InputVertex.h"
#include <vector>
#include "Surface.h"

class Model
{
public:
	Model(const std::wstring& model, const std::wstring& texture);
	~Model() = default;;
	std::vector<InputVertex> getVertices() const;
	const Surface* getSurface() const;
private:
	std::vector<InputVertex> vertices_;
	Surface surface_;
};

