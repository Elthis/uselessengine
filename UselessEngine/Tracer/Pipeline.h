#pragma once

#include "UselessEngine.h"
#include "ScreenTransformer.h"
#include "VertexShader.h"
#include "PixelShader.h"
#include "InputVertex.h"
#ifdef WIN32
#define NOMINMAX
#undef min
#undef max
#endif
#include <vector>
#include <optional>
#include <algorithm>


template <class Vertex>
class Pipeline
{
	static_assert(std::is_base_of<InputVertex, Vertex>::value == 1, "Pipeline vertex must be derived from InputVertex");
	class Triangle
	{
	public:
		Triangle(Vertex v0, Vertex v1, Vertex v2) :
			v0(v0),
			v1(v1),
			v2(v2)
		{}
		Vertex v0;
		Vertex v1;
		Vertex v2;
	};
public:
	explicit Pipeline(UselessEngine& gfx);
	~Pipeline()=default;
	void drawTriangle(std::vector<Vertex> vertices)const;
	void setVertexShader(const VertexShader<Vertex>* shader);
	void setPixelShader(const PixelShader<Vertex>* shader);
private:
	void vertexTransformer(std::vector<Vertex>& vertices)const;
	void triangleAssembler(Vertex& v0, Vertex& v1, Vertex& v2) const;
	void clipTriangle(Triangle& t) const;
	void processTriangle(Triangle& triangle) const;
	void triangleRasterizer(const Triangle& triangle) const;
	void screenTransformer(Vertex& vertex) const;
	void drawBottomTriangle(const Vertex& v0, const Vertex& v1, const Vertex& v2) const;
	void drawTopTriangle(const Vertex& v0, const Vertex& v1, const Vertex& v2)const;
	void drawScanline(int y, int startX, int endX, const Vertex& stepX, Vertex& iLine) const;


	UselessEngine& gfx_;
	ScreenTransformer screenTransformer_;
	const VertexShader<Vertex>* vertexShader_;
	const PixelShader<Vertex>* pixelShader_;
};


template <class Vertex>
Pipeline<Vertex>::Pipeline(UselessEngine& gfx):
	gfx_(gfx),
	screenTransformer_(gfx.getWidth(), gfx.getHeight()),
	vertexShader_(nullptr), 
	pixelShader_(nullptr)
{
}

template <class Vertex>
void Pipeline<Vertex>::drawTriangle(std::vector<Vertex> vertices) const
{
	vertexTransformer(vertices);
}

template <class Vertex>
void Pipeline<Vertex>::setVertexShader(const VertexShader<Vertex>* shader)
{
	vertexShader_ = shader;
}

template <class Vertex>
void Pipeline<Vertex>::setPixelShader(const PixelShader<Vertex>* shader)
{
	pixelShader_ = shader;
}


template <class Vertex>
void Pipeline<Vertex>::vertexTransformer(std::vector<Vertex>& vertices) const
{
	for (auto i = vertices.begin(), end = vertices.end(); i < end; std::advance(i, 3))
	{
		Vertex& v0 = *i;
		Vertex& v1 = *std::next(i, 1);
		Vertex& v2 = *std::next(i, 2);
		vertexShader_->process(v0);
		vertexShader_->process(v1);
		vertexShader_->process(v2);
		triangleAssembler(v0, v1, v2);
	}
}

template <class Vertex>
void Pipeline<Vertex>::triangleAssembler(Vertex& v0, Vertex& v1, Vertex& v2) const
{
		
		const auto triangleNormal = (v1.pos - v0.pos).getCrossProduct(v2.pos - v0.pos);
		if (triangleNormal.xyz().getDotProduct(v0.pos.xyz()) < 0.0f)
		{
			auto triangle = Triangle(v0, v1, v2);
			clipTriangle(triangle);
		}

}

template <class Vertex>
void Pipeline<Vertex>::clipTriangle(Triangle& t) const
{
	if (t.v0.pos.x > t.v0.pos.w &&
		t.v1.pos.x > t.v1.pos.w &&
		t.v2.pos.x > t.v2.pos.w)
		return;
	if (t.v0.pos.x < -t.v0.pos.w &&
		t.v1.pos.x < -t.v1.pos.w &&
		t.v2.pos.x < -t.v2.pos.w)
		return;
	if (t.v0.pos.y > t.v0.pos.w &&
		t.v1.pos.y > t.v1.pos.w &&
		t.v2.pos.y > t.v2.pos.w)
		return;
	if (t.v0.pos.y < -t.v0.pos.w &&
		t.v1.pos.y < -t.v1.pos.w &&
		t.v2.pos.y < -t.v2.pos.w)
		return;
	if (t.v0.pos.z > t.v0.pos.w &&
		t.v1.pos.z > t.v1.pos.w &&
		t.v2.pos.z > t.v2.pos.w)
		return;
	if (t.v0.pos.z < 0.0f &&
		t.v1.pos.z < 0.0f&&
		t.v2.pos.z < 0.0f)
		return;

	/**
	* \brief Clips triangle when one vertex is outside view space
	* \param v0 Vertex outside the view space
	* \param v1 Vertex inside the view space
	* \param v2 Vertex inside the view space
	*/
	auto ClipOne = [this](Vertex& v0, Vertex& v1, Vertex& v2)
	{
		auto alphaOne = (-v0.pos.z) / (v1.pos.z - v0.pos.z);
		auto alphaTwo = (-v0.pos.z) / (v2.pos.z - v0.pos.z);
		Vertex v3 = v0.getInterpolated(alphaOne, v1);
		v0.interpolateTo(alphaTwo, v2);
		auto firstTriangle = Triangle(v3, v1, v2);
		auto secondTriangle = Triangle(v0, v3, v2);
		processTriangle(firstTriangle);
		processTriangle(secondTriangle);
	};
	/**
	* \brief Clips triangle when one vertex is outside view space
	* \param v0 Vertex outside the view space
	* \param v1 Vertex outside the view space
	* \param v2 Vertex inside the view space
	*/
	auto ClipTwo = [this](Vertex & v0, Vertex & v1, Vertex & v2)
	{
		auto alphaOne = (-v0.pos.z) / (v2.pos.z - v0.pos.z);
		auto alphaTwo = (-v1.pos.z) / (v2.pos.z - v1.pos.z);
		v0.interpolateTo(alphaOne, v2);
		v1.interpolateTo(alphaTwo, v2);
		auto triangle = Triangle(v0, v1, v2);
		processTriangle(triangle);
	};
	if (t.v0.pos.z < 0.0f)
	{
		if (t.v1.pos.z < 0.0f)
		{
			ClipTwo(t.v0, t.v1, t.v2);
		}
		else if (t.v2.pos.z < 0.0f)
		{
			ClipTwo(t.v0, t.v2, t.v1);
		}
		else
		{
			ClipOne(t.v0, t.v1, t.v2);
		}
	}
	else if (t.v1.pos.z < 0.0f)
	{
		if (t.v2.pos.z < 0.0f)
			ClipTwo(t.v1, t.v2, t.v0);
		else
			ClipOne(t.v1, t.v0, t.v2);
	}
	else if (t.v2.pos.z < 0.0f)
		ClipOne(t.v2, t.v1, t.v0);
	else
	{
		processTriangle(t);
	}
	

}


template <class Vertex>
void Pipeline<Vertex>::processTriangle(Triangle& triangle) const
{
	screenTransformer(triangle.v0);
	screenTransformer(triangle.v1);
	screenTransformer(triangle.v2);
	triangleRasterizer(triangle);
}



template <class Vertex>
void Pipeline<Vertex>::triangleRasterizer(const Triangle& triangle) const
{
	//Pointers to swap vertices
	const Vertex* pV0 = &triangle.v0;
	const Vertex* pV1 = &triangle.v1;
	const Vertex* pV2 = &triangle.v2;

	// Achieving V0 <= V1 <= V2 order
	if (pV0->pos.y > pV1->pos.y) std::swap(pV0, pV1);
	if (pV1->pos.y > pV2->pos.y) std::swap(pV2, pV1);
	if (pV0->pos.y > pV1->pos.y) std::swap(pV1, pV0);

	//Flat Bottom Triangle
	if (pV1->pos.y == pV2->pos.y)
	{
		//Assure pV1 points to most left vertex
		if (pV1->pos.x > pV2->pos.x) std::swap(pV1, pV2);

		drawBottomTriangle(*pV1, *pV0, *pV2);
	}
	//Flat Top Triangle
	else if (pV0->pos.y == pV1->pos.y)
	{
		//Assure pV0 points to most left vertex
		if (pV0->pos.x > pV1->pos.x) std::swap(pV0, pV1);

		drawTopTriangle(*pV1, *pV2, *pV0);
	}
	//Typical triangle
	else
	{
		const float alpha = (pV1->pos.y - pV0->pos.y) / (pV2->pos.y - pV0->pos.y);
		const auto splitVec = pV0->getInterpolated(alpha, *pV2);

		//Check if triangle is major right
		if (pV1->pos.x < splitVec.pos.x)
		{
			drawBottomTriangle(*pV1, *pV0, splitVec);
			drawTopTriangle(splitVec, *pV2, *pV1);
		}
		else
		{
			drawBottomTriangle(splitVec, *pV0, *pV1);
			drawTopTriangle(*pV1, *pV2, splitVec);
		}
	}
}

template <class Vertex>
void Pipeline<Vertex>::screenTransformer(Vertex& vertex) const
{
	screenTransformer_.transform(vertex);
}

/**
 * \brief Draws flat bottom triangle
 * \param v0 Bottom left vertex
 * \param v1 Top vertex
 * \param v2 Bottom right vertex
 */
template <class Vertex>
void Pipeline<Vertex>::drawBottomTriangle(const Vertex& v0, const Vertex& v1, const Vertex& v2) const
{
	
	//Calculate slope
	const float deltaY = (v0.pos.y - v1.pos.y);
	const float m0 = (v0.pos.x - v1.pos.x) / deltaY;
	const float m1 = (v2.pos.x - v1.pos.x) / deltaY;

	const int startY = std::max(static_cast<int>(ceil(v1.pos.y - 0.5f)), 0);
	const int endY = std::min(static_cast<int>(ceil(v2.pos.y - 0.5f)), static_cast<int>(gfx_.getHeight()-1));
	
	const Vertex stepLeftY = (v0 - v1) / deltaY;
	const Vertex stepRightY = (v2 - v1) / deltaY;
	Vertex uvLeft = v1 + stepLeftY * (float(startY) + 0.5f - v1.pos.y);
	Vertex uvRight = v1 + stepRightY * (float(startY) + 0.5f - v1.pos.y);

	for (auto y = startY; y < endY; y++, uvLeft += stepLeftY, uvRight += stepRightY)
	{

		//Calculate left and right pixel x cords
		const float pixel0 = m0 * (float(y) + 0.5f - v1.pos.y) + v1.pos.x;
		const float pixel1 = m1 * (float(y) + 0.5f - v1.pos.y) + v1.pos.x;

		//Calculate screen start and end x cords
		const int startX = std::max(static_cast<int>(ceil(pixel0 - 0.5f)), 0);
		const int endX = std::min(static_cast<int>(ceil(pixel1 - 0.5f)), static_cast<int>(gfx_.getWidth()-1));

		const Vertex stepX = (uvRight - uvLeft) / (pixel1 - pixel0);
		Vertex uvScan = uvLeft + stepX * (float(startX) + 0.5f - pixel0);
		drawScanline(y, startX, endX, stepX, uvScan);

	}
}


template <class Vertex>
void Pipeline<Vertex>::drawTopTriangle(const Vertex& v0, const Vertex& v1, const Vertex& v2) const
{
	//Calculate slope
	const float deltaY = v1.pos.y - v2.pos.y;
	const float m0 = (v1.pos.x - v2.pos.x) / deltaY;
	const float m1 = (v1.pos.x - v0.pos.x) / deltaY;

	const int startY = std::max(static_cast<int>(ceil(v2.pos.y - 0.5f)), 0);
	const int endY = std::min(static_cast<int>(ceil(v1.pos.y - 0.5f)), static_cast<int>(gfx_.getHeight()-1));

	const Vertex stepLeftY = (v1 - v2) / deltaY;
	const Vertex stepRightY = (v1 - v0) / deltaY;
	Vertex uvLeft = v2 + stepLeftY * (float(startY) + 0.5f - v2.pos.y);
	Vertex uvRight = v0 + stepRightY * (float(startY) + 0.5f - v2.pos.y);

	for (auto y = startY; y < endY; y++, uvLeft += stepLeftY, uvRight += stepRightY)
	{
		//Calculate left and right pixel x cords
		const float pixel0 = m0 * (float(y) + 0.5f - v2.pos.y) + v2.pos.x;
		const float pixel1 = m1 * (float(y) + 0.5f - v2.pos.y) + v0.pos.x;

		//Calculate screen start and end x cords
		const int startX = std::max(static_cast<int>(ceil(pixel0 - 0.5f)), 0);
		const int endX = std::min(static_cast<int>(ceil(pixel1 - 0.5f)), static_cast<int>(gfx_.getWidth()-1));

		const Vertex stepX = (uvRight - uvLeft) / (pixel1 - pixel0);
		Vertex uvScan = uvLeft + stepX * (float(startX) + 0.5f - pixel0);
		drawScanline(y, startX, endX, stepX, uvScan);
	
	}
}

template <class Vertex>
void Pipeline<Vertex>::drawScanline(const int y, const int startX, const int endX, const Vertex& stepX, Vertex& iLine) const
{
	for (auto x = startX; x < endX; x++, iLine += stepX)
	{
		if (gfx_.checkZBuffer(x, y, iLine.pos.z))
		{
			const float w = 1.0f / iLine.pos.w;
			Vertex pixelData = iLine;
			pixelData.uv *= w;
			pixelData.normal *= w;
			gfx_.putPixel(x, y, pixelShader_->process(pixelData));
		}
	}
}
