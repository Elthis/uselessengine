#pragma once
#include "Color.h"
#include "InputVertex.h"
template<class Vertex>
class PixelShader
{
	static_assert(std::is_base_of<InputVertex, Vertex>::value == 1, "Pipeline vertex must be derived from Vertex");
public:
	PixelShader() = default;
	virtual ~PixelShader() = default;
	virtual Color process(const Vertex& vertex) const = 0;
};