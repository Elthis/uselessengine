#pragma once
#include "Color.h"
#include "Surface.h"
#ifdef WIN32
#define NOMINMAX
#undef min
#undef max
#endif
#include <algorithm>

class Sampler
{
public:
	Sampler() {}
	static Color getBilinearFilteredPixelColor(const Surface& surface, float u, float v)
	{
		const int width = surface.getWidth()-1;
		const int height = surface.getHeight()-1;
		u *= width;
		v *= height;
		const int x = std::max(floorf(u), 0.0f);
		const int y = std::max(floorf(v), 0.0f);
		const float u_ratio = u - x;
		const float v_ratio = v - y;
		const float u_opposite = 1 - u_ratio;
		const float v_opposite = 1 - v_ratio;
		auto result = (surface.getPixel(std::min(x, width), std::min(y, height)) * u_opposite + surface.getPixel(std::min(x+1, width), std::min(y, height)) * u_ratio) * v_opposite +
			(surface.getPixel(std::min(x, width), std::min(y+1, height)) * u_opposite + surface.getPixel(std::min(x + 1, width), std::min(y + 1, height))* u_ratio) * v_ratio;
		return result;
	}
	~Sampler();
};

