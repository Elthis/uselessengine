#pragma once
class ScreenTransformer
{
public:
	ScreenTransformer(const size_t width, const size_t height): 
		xFactor_(width / 2.0f),
		yFactor_(height / 2.0f)
	{}
	~ScreenTransformer() = default;
	template<class Vertex> 
	Vertex& transform(Vertex& vec) const;
	template<typename Vertex>
	Vertex getTransform(const Vertex& vec) const;

private:
	float xFactor_;
	float yFactor_;
};

template <class Vertex>
Vertex& ScreenTransformer::transform(Vertex& vec) const
{
	const auto wInv = fabs(1.0f / vec.pos.w);
	vec *= wInv;
	vec.pos.x = (vec.pos.x + 1.0f)*xFactor_;
	vec.pos.y = (-vec.pos.y + 1.0f)*yFactor_;
	vec.pos.w = wInv;
	return vec;
}

template <typename Vertex>
Vertex ScreenTransformer::getTransform(const Vertex& vec) const
{
	auto v(vec);
	return transform(v);
}

