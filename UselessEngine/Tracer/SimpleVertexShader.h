#pragma once
#include "VertexShader.h"
#include "Matrix.h"
template<class Vertex>
class SimpleVertexShader final :
	public VertexShader<Vertex>
{
public:
	SimpleVertexShader():
	transformationMatrix_(Math3D::Matrix4f::identity())
	{
	};
	virtual ~SimpleVertexShader() = default;
	void process(Vertex& vertex) const override;
	void setTransformationMatrix(const Math3D::Matrix4f& mat);
	void setViewProjectionMatrix(const Math3D::Matrix4f& mat);
private:
	Math3D::Matrix4f transformationMatrix_;
	Math3D::Matrix4f viewProjectionMatrix_;
};

template <class Vertex>
void SimpleVertexShader<Vertex>::process(Vertex& vertex) const
{
	vertex.pos = viewProjectionMatrix_*(transformationMatrix_ * vertex.pos);
	vertex.normal = (transformationMatrix_ * Math3D::Vec4(vertex.normal, 0.0f)).xyz();
	vertex.normal.normalize();
}

template <class Vertex>
void SimpleVertexShader<Vertex>::setTransformationMatrix(const Math3D::Matrix4f& mat)
{
	transformationMatrix_ = mat;
}

template <class Vertex>
void SimpleVertexShader<Vertex>::setViewProjectionMatrix(const Math3D::Matrix4f& mat)
{
	viewProjectionMatrix_ = mat;
}

