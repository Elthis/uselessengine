#include "Surface.h"
#include <stdexcept>

#include <Windows.h>
#include <gdiplus.h>
#include <memory>
#pragma comment( lib,"gdiplus.lib" )

Surface::Surface(const std::wstring& path)
{
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR           gdiplusToken;
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, nullptr);
	{
		Gdiplus::Bitmap bitmap(path.c_str());
		if (bitmap.GetLastStatus() != Gdiplus::Status::Ok)
			throw std::runtime_error("Failed to open file: " + std::string((char*)(path.data())));

		width_ = bitmap.GetWidth();
		height_ = bitmap.GetHeight();
		bytes_ = std::make_unique<Color[]>(width_ * height_);

		for (unsigned int y = 0; y < height_; y++)
		{
			for (unsigned int x = 0; x < width_; x++)
			{
				Gdiplus::Color c;
				bitmap.GetPixel(x, y, &c);
				bytes_[y * width_ + x] = Color(c.GetR(), c.GetG(), c.GetB());
			}
		}
	}
	Gdiplus::GdiplusShutdown(gdiplusToken);

}

Surface::Surface(const Surface& other):
width_(other.width_),
height_(other.height_),
bytes_(std::make_unique<Color[]>(other.height_*other.width_))
{
	memcpy(bytes_.get(), other.bytes_.get(), width_ * height_ * sizeof(Color));
}

Surface::Surface(Surface&& other) noexcept:
width_(other.width_),
height_(other.height_),
bytes_(std::move(other.bytes_))
{
	other.width_ = 0;
	other.height_ = 0;
}


size_t Surface::getWidth() const
{
	return width_;
}

size_t Surface::getHeight() const
{
	return height_;
}

const Color* Surface::getBuffer() const
{
	return bytes_.get();
}

Surface& Surface::operator=(const Surface& other)
{
	*this = Surface(other);
	return *this;
}

Surface& Surface::operator=(Surface&& other) noexcept
{
	width_ = other.width_;
	height_ = other.height_;
	other.width_ = 0;
	other.height_ = 0;
	memcpy(bytes_.get(), other.bytes_.get(), width_ * height_ * sizeof(Color));
	return *this;
}
