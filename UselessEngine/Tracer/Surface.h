#pragma once
#include <memory>
#include <string>
#include "Color.h"

/**
 * \brief Class designed to load and store *.bmp file information
 */
class Surface
{
public:
	explicit Surface(const std::wstring& path);
	Surface(const Surface& other);
	Surface(Surface&& other) noexcept;
	~Surface() = default;
	/**
	 * \brief 
	 * \param x X coordinate of a pixel
	 * \param y Y coordinate of a pixel
	 * \return Color at (x,y)
	 */
	Color getPixel(const size_t x, const size_t y) const
	{
		return bytes_[y * width_ + x];
	}
	size_t getWidth() const;
	size_t getHeight() const;
	const Color* getBuffer() const;
	Surface& operator=(const Surface& other);
	Surface& operator=(Surface&& other) noexcept;
private:
	size_t width_{};
	size_t height_{};
	std::unique_ptr<Color[]> bytes_;
};

