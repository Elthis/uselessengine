#pragma once
#include "PixelShader.h"
#include "Surface.h"
#include "Sampler.h"

template<class Vertex>
class TexturePixelShader final :
	public PixelShader<Vertex>
{
public:
	TexturePixelShader();
	virtual ~TexturePixelShader() = default;
	Color process(const Vertex& vertex) const override;
	void setTexture(const Surface* texture);
	void setDiffuseLightDirection(const Math3D::Vec3& dir);
	void setAmbientLight(float ambientLight);
private:
	const Surface* texture_;
	float textureWidth_;
	float textureHeight_;
	Math3D::Vec3 diffuseLightDir_;
	float ambientLight_;
};

template <class Vertex>
TexturePixelShader<Vertex>::TexturePixelShader():
	texture_(nullptr), 
	textureWidth_(0), 
	textureHeight_(0)
{
}

template <class Vertex>
Color TexturePixelShader<Vertex>::process(const Vertex& vertex) const
{

	float intens = std::clamp(std::clamp((-diffuseLightDir_).normalize().getDotProduct(vertex.normal), 0.0f, 1.0f) + ambientLight_, 0.0f, 1.0f);
	Color pixel = texture_->getPixel(int(std::min(vertex.uv.x*textureWidth_, textureWidth_)), int(std::min(vertex.uv.y*textureHeight_, textureHeight_)))*intens;
	//Color pixel = Sampler::getBilinearFilteredPixelColor(*texture_, vertex.uv.x, vertex.uv.y)*intens;
	return pixel;
}

template <class Vertex>
void TexturePixelShader<Vertex>::setTexture(const Surface* texture)
{
	texture_ = texture;
	textureWidth_ = texture->getWidth() - 1;
	textureHeight_ = texture->getHeight() - 1;
}

template <class Vertex>
void TexturePixelShader<Vertex>::setDiffuseLightDirection(const Math3D::Vec3& dir)
{
	diffuseLightDir_ = dir;
}

template <class Vertex>
void TexturePixelShader<Vertex>::setAmbientLight(float ambientLight)
{
	ambientLight_ = ambientLight;
}


