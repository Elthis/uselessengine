#include "TexturedCube.h"

using Math3D::Vec3, Math3D::Vec2;

TexturedCube::TexturedCube(const float size, const std::wstring& path):
surface_(path)
{
	const auto side = size / 2.0f;
	/**/
	//Front
	vertices_.emplace_back(Vec3(-side, -side, -side), Vec2(0.0f, 1.0f), Vec3(0.0f, 0.0f, -1.0f));
	vertices_.emplace_back(Vec3(-side, side, -side), Vec2(0.0f, 0.0f), Vec3(0.0f, 0.0f, -1.0f));
	vertices_.emplace_back(Vec3(side, -side, -side), Vec2(1.0f, 1.0f), Vec3(0.0f, 0.0f, -1.0f));

	vertices_.emplace_back(Vec3(-side, side, -side), Vec2(0.0f, 0.0f), Vec3(0.0f, 0.0f, -1.0f));
	vertices_.emplace_back(Vec3(side, side, -side), Vec2(1.0f, 0.0f), Vec3(0.0f, 0.0f, -1.0f));
	vertices_.emplace_back(Vec3(side, -side, -side), Vec2(1.0f, 1.0f), Vec3(0.0f, 0.0f, -1.0f));

	//Back
	vertices_.emplace_back(Vec3(side, -side, side), Vec2(0.0f, 1.0f), Vec3(0.0f, 0.0f, 1.0f));
	vertices_.emplace_back(Vec3(side, side, side), Vec2(0.0f, 0.0f), Vec3(0.0f, 0.0f, 1.0f));
	vertices_.emplace_back(Vec3(-side, -side, side), Vec2(1.0f, 1.0f), Vec3(0.0f, 0.0f, 1.0f));

	vertices_.emplace_back(Vec3(side, side, side), Vec2(0.0f, 0.0f), Vec3(0.0f, 0.0f, 1.0f));
	vertices_.emplace_back(Vec3(-side, side, side), Vec2(1.0f, 0.0f), Vec3(0.0f, 0.0f, 1.0f));
	vertices_.emplace_back(Vec3(-side, -side, side), Vec2(1.0f, 1.0f), Vec3(0.0f, 0.0f, 1.0f));

	//Right
	vertices_.emplace_back(Vec3(side, -side, -side), Vec2(0.0f, 1.0f), Vec3(1.0f, 0.0f, 0.0f));
	vertices_.emplace_back(Vec3(side, side, -side), Vec2(0.0f, 0.0f), Vec3(1.0f, 0.0f, 0.0f));
	vertices_.emplace_back(Vec3(side, -side, side), Vec2(1.0f, 1.0f), Vec3(1.0f, 0.0f, 0.0f));

	vertices_.emplace_back(Vec3(side, side, -side), Vec2(0.0f, 0.0f), Vec3(1.0f, 0.0f, 0.0f));
	vertices_.emplace_back(Vec3(side, side, side), Vec2(1.0f, 0.0f), Vec3(1.0f, 0.0f, 0.0f));
	vertices_.emplace_back(Vec3(side, -side, side), Vec2(1.0f, 1.0f), Vec3(1.0f, 0.0f, 0.0f));

	//Left
	vertices_.emplace_back(Vec3(-side, -side, side), Vec2(0.0f, 1.0f), Vec3(-1.0f, 0.0f, 0.0f));
	vertices_.emplace_back(Vec3(-side, side, side), Vec2(0.0f, 0.0f), Vec3(-1.0f, 0.0f, 0.0f));
	vertices_.emplace_back(Vec3(-side, -side, -side), Vec2(1.0f, 1.0f), Vec3(-1.0f, 0.0f, 0.0f));

	vertices_.emplace_back(Vec3(-side, side, side), Vec2(0.0f, 0.0f), Vec3(-1.0f, 0.0f, 0.0f));
	vertices_.emplace_back(Vec3(-side, side, -side), Vec2(1.0f, 0.0f), Vec3(-1.0f, 0.0f, 0.0f));
	vertices_.emplace_back(Vec3(-side, -side, -side), Vec2(1.0f, 1.0f), Vec3(-1.0f, 0.0f, 0.0f));

	//Up
	vertices_.emplace_back(Vec3(-side, side, -side), Vec2(0.0f, 1.0f), Vec3(0.0f, 1.0f, 0.0f));
	vertices_.emplace_back(Vec3(-side, side, side), Vec2(0.0f, 0.0f), Vec3(0.0f, 1.0f, 0.0f));
	vertices_.emplace_back(Vec3(side, side, -side), Vec2(1.0f, 1.0f), Vec3(0.0f, 1.0f, 0.0f));

	vertices_.emplace_back(Vec3(-side, side, side), Vec2(0.0f, 0.0f), Vec3(0.0f, 1.0f, 0.0f));
	vertices_.emplace_back(Vec3(side, side, side), Vec2(1.0f, 0.0f), Vec3(0.0f, 1.0f, 0.0f));
	vertices_.emplace_back(Vec3(side, side, -side), Vec2(1.0f, 1.0f), Vec3(0.0f, 1.0f, 0.0f));

	//Down
	vertices_.emplace_back(Vec3(side, -side, -side), Vec2(0.0f, 1.0f), Vec3(0.0f, -1.0f, 0.0f));
	vertices_.emplace_back(Vec3(side, -side, side), Vec2(0.0f, 0.0f), Vec3(0.0f, -1.0f, 0.0f));
	vertices_.emplace_back(Vec3(-side, -side, -side), Vec2(1.0f, 1.0f), Vec3(0.0f, -1.0f, 0.0f));

	vertices_.emplace_back(Vec3(side, -side, side), Vec2(0.0f, 0.0f), Vec3(0.0f, -1.0f, 0.0f));
	vertices_.emplace_back(Vec3(-side, -side, side), Vec2(1.0f, 0.0f), Vec3(0.0f, -1.0f, 0.0f));
	vertices_.emplace_back(Vec3(-side, -side, -side), Vec2(1.0f, 1.0f), Vec3(0.0f, -1.0f, 0.0f));


}

std::vector<InputVertex> TexturedCube::getVertices() const
{
	return vertices_;
}

const Surface& TexturedCube::getSurface() const
{
	return surface_;
}

