#pragma once
#include "Surface.h"
#include "InputVertex.h"
#include <vector>

class TexturedCube
{
public:
	TexturedCube(float size, const std::wstring& path);
	~TexturedCube() = default;
	std::vector<InputVertex>  getVertices() const;
	const Surface& getSurface() const;
private:
	Surface surface_;
	std::vector<InputVertex> vertices_;
};

