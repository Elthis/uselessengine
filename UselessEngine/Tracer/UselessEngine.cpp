#include "UselessEngine.h"
#include <algorithm>
#include <fstream>
#include <sstream>

using Math3D::Vec2;

UselessEngine::UselessEngine(HWND hwnd, const size_t width, const size_t height):
	hwnd_(hwnd),
	present_(false),
	join_(false),
	width_(width),
	height_(height)
{
	worker_ = std::thread([this]
	{
		while (true)
		{
			std::unique_lock<std::mutex> lk(workerFence_);
			workerEvent_.wait(lk, [this] {return this->present_ || this->join_; });
			if (present_)
			{
				const auto hdc = GetDC(hwnd_);
				StretchDIBits(hdc, 0, 0, width_, height_, 0, 0, width_, height_, presentBuffer_.get(), &backBufferInfo_, DIB_RGB_COLORS, SRCCOPY);
				ReleaseDC(hwnd_, hdc);
				present_ = false;
			}
			else if (join_)
				break;
		}
	});
	createBackBuffer(width, height);
}


UselessEngine::~UselessEngine()
{
	workerFence_.lock();
	join_ = true;
	workerFence_.unlock();
	workerEvent_.notify_one();
	worker_.join();
}

void UselessEngine::clearScreen(const uint8_t r, const uint8_t g, const uint8_t b) const
{
	std::fill_n(backBuffer_.get(), height_ * width_, Color(r, g, b));
	std::fill_n(zBuffer_.get(), height_ * width_, 1.0);
}

void UselessEngine::swapBuffer()
{
	workerFence_.lock();
	present_ = true;
	std::swap(backBuffer_, presentBuffer_);
	workerFence_.unlock();
	workerEvent_.notify_one();
	
}


size_t UselessEngine::getWidth() const
{
	return width_;
}

size_t UselessEngine::getHeight() const
{
	return height_;
}


void UselessEngine::drawLine(const int x0, const int y0, const int x1, const int y1, const Color & col) const
{
	const int deltaX = x1 - x0;
	const int deltaY = y1 - y0;
	if (abs(deltaY) < abs(deltaX))
		if (x0 > x1)
			drawLowLine(x1, y1, x0, y0, col);
		else
			drawLowLine(x0, y0, x1, y1, col);
	else
		if (y0 > y1)
			drawHighLine(x1, y1, x0, y0, col);
		else
			drawHighLine(x0, y0, x1, y1, col);
}

void UselessEngine::drawLine(const Vec2& v1, const Vec2& v2, const Color & col) const
{
	drawLine(v1.x, v1.y, v2.x, v2.y, col);
}

void UselessEngine::drawLowLine(const int x0, const int y0, const int x1, const int y1, const Color & col) const
{
	const int deltaX = x1 - x0;
	int deltaY = y1 - y0;
	int yi = 1;
	if (deltaY < 0)
	{
		yi = -1;
		deltaY = -deltaY;
	}
	int D = 2 * deltaY - deltaX;
	int y = y0;

	for (auto x = x0; x <= x1; x++)
	{
		putPixel(x, y, col);
		if (D > 0)
		{
			y = y + yi;
			D = D - 2 * deltaX;
		}
		D = D + 2 * deltaY;
	}
}

void UselessEngine::drawHighLine(const int x0, const int y0, const int x1, const int y1, const Color & col) const
{
	int deltaX = x1 - x0;
	const int deltaY = y1 - y0;
	int xi = 1;
	if (deltaX < 0)
	{
		xi = -1;
		deltaX = -deltaX;
	}
	int D = 2 * deltaX - deltaY;
	int x = x0;

	for (auto y = y0; y <= y1; y++)
	{
		putPixel(x, y, col);
		if (D > 0)
		{
			x = x + xi;
			D = D - 2 * deltaY;
		}
		D = D + 2 * deltaX;
	}
}



void UselessEngine::createBackBuffer(const size_t width, const size_t height)
{
	int lineWidth = width;

	const int widthMod4 = width % 4;

	if (widthMod4 > 0)
	{
		lineWidth += 4 - widthMod4;
	}
	
	backBufferInfo_ = {};
	backBufferInfo_.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	backBufferInfo_.bmiHeader.biPlanes = 1;
	backBufferInfo_.bmiHeader.biBitCount = 24;
	backBufferInfo_.bmiHeader.biCompression = BI_RGB;
	backBufferInfo_.bmiHeader.biWidth = lineWidth;
	backBufferInfo_.bmiHeader.biHeight = height;
	backBuffer_ = std::make_unique<Color[]>(width*height);
	presentBuffer_ = std::make_unique<Color[]>(width*height);
	zBuffer_ = std::make_unique<float[]>(width*height);
}	

