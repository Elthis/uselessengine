#pragma once
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <Windows.h>
#include <cstdint>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include "Color.h"
#include "Vectors.h"

typedef std::unique_ptr<Color[]> BackBuffer;
typedef BITMAPINFO BackBufferInfo;

/**
 * \brief UselessEngine manages swapchain and writing to BackBuffer
 */
class UselessEngine
{
public:
	/**
	 * \param hwnd Handle of a window
	 * \param width Width of client space
	 * \param height Height of client space
	 */
	UselessEngine(HWND hwnd, size_t width, size_t height);
	~UselessEngine();
	/**
	 * \brief Clears Back Buffer and Z Buffer using byte values (0-255)
	 * \param r Red
	 * \param g Green
	 * \param b Blue
	 */
	void clearScreen(uint8_t r=0, uint8_t g=0, uint8_t b=0) const;

	/**
	 * \brief Swaps buffers and orders worker to present freshly rendered frame
	 */
	void swapBuffer();

	/**
	 * \brief Puts pixel at specific (x,y)
	 * \param x x Coordinate
	 * \param y y Coordinate
	 * \param col Color of a pixel
	 */
	void putPixel(size_t x, size_t y, const Color& col) const;

	/**
	 * \brief Draws line between two points
	 * \param x0 begin x
	 * \param y0 begin y
	 * \param x1 end x
	 * \param y1 end y
	 * \param col Color of a line
	 */
	void drawLine(int x0, int y0, int x1, int y1, const Color& col) const;

	/**
	 * \brief Draws line between two points
	 * \param v1 First point
	 * \param v2 Second point
	 * \param col Color of a line
	 */
	void drawLine(const Math3D::Vec2& v1, const Math3D::Vec2& v2, const Color& col) const;
	size_t getWidth() const;
	size_t getHeight() const;
	/**
	 * \brief Checks if pixel will be visible, if it is sets ZBuffer at (x,y) to it's z value
	 * \param x Coordinates X
	 * \param y Coordinates Y
	 * \param z Normalized Z distance from Camera
	 * \return Returns true if pixel should be drawn
	 */
	bool checkZBuffer(size_t x, size_t y, float z) const;
private:
	/**
	 * \brief Draws line with bigger x difference between points
	 * \param x0 begin x
	 * \param y0 begin y
	 * \param x1 end x
	 * \param y1 end y
	 * \param col Color of a line
	 */
	void drawLowLine(int x0, int y0, int x1, int y1, const Color& col) const;

	/**
	 * \brief Draws line with bigger y difference between points
	 * \param x0 begin x
	 * \param y0 begin y
	 * \param x1 end x
	 * \param y1 end y
	 * \param col Color of a line
	 */
	void drawHighLine(int x0, int y0, int x1, int y1, const Color& col) const;
	/**
	 * \brief Creates FrontBuffer, BackBuffer and ZBuffer;
	 * \param width width of the buffer(image)
	 * \param height height of the buffer(image)
	 */
	void createBackBuffer(size_t width, size_t height);
	HWND hwnd_;
	std::condition_variable workerEvent_;
	bool present_;
	bool join_;
	std::mutex workerFence_;
	std::thread worker_;
	BackBuffer backBuffer_;
	BackBuffer presentBuffer_;
	BackBufferInfo backBufferInfo_{};
	size_t width_;
	size_t height_;
	std::unique_ptr<float[]> zBuffer_;
};

inline void UselessEngine::putPixel(const size_t x, const size_t y, const Color& col) const
{
	backBuffer_[((height_ - y - 1)*width_ + x)] = col;
}

inline bool UselessEngine::checkZBuffer(const size_t x, const size_t y, const float z) const
{
	if (zBuffer_[y*width_ + x] > z)
	{
		zBuffer_[y*width_ + x] = z;
		return true;
	}
	return false;
}
