#pragma once
#include <cmath>

namespace Math3D
{
	__declspec(align(16)) 
	class Vec2
	{
	public:
		Vec2(const float x = 0.0f, const float y = 0.0f) :
			x(x),
			y(y)
		{}

		Vec2 operator+(const Vec2& rhs) const
		{
			return Vec2(x + rhs.x, y + rhs.y);
		}

		Vec2 operator-(const Vec2& rhs) const
		{
			return Vec2(x - rhs.x, y - rhs.y);
		}

		Vec2 operator*(const float scalar) const
		{
			return Vec2(x*scalar, y*scalar);
		}

		Vec2 operator/(const float divider) const
		{
			return Vec2(x / divider, y / divider);
		}

		Vec2& operator+=(const Vec2& rhs)
		{
			x += rhs.x;
			y += rhs.y;
			return *this;
		}

		Vec2& operator-=(const Vec2& rhs)
		{
			x -= rhs.x;
			y -= rhs.y;
			return *this;
		}

		Vec2& operator*=(const float scalar)
		{
			x *= scalar;
			y *= scalar;
			return *this;
		}

		Vec2& operator/=(const float divider)
		{
			x /= divider;
			y /= divider;
			return *this;
		}

		Vec2 operator-() const
		{
			return Vec2(*this).neg();
		}

		float getDotProduct(const Vec2& vec) const
		{
			return x * vec.x + y * vec.y;
		}

		float getLength() const
		{
			return sqrtf(getDotProduct(*this));
		}

		Vec2& normalize()
		{
			const auto length = getLength();
			x /= length;
			y /= length;
			return *this;
		}

		Vec2 getNormalized() const
		{
			return Vec2(*this).normalize();
		}

		Vec2& interpolate(const float alpha, const Vec2& interpolateTo)
		{
			return *this = (interpolateTo - *this)*alpha + *this;
		}

		Vec2 getInterpolated(const float alpha, const Vec2& interpolateTo) const
		{
			return Vec2(*this).interpolate(alpha, interpolateTo);
		}

		Vec2& neg()
		{
			x = -x;
			y = -y;
			return *this;
		}

		union
		{
			float element[2]{};
			struct
			{
				float x;
				float y;
			};
		};
	};

	__declspec(align(16))
	class Vec3
	{
	public:
		Vec3(float x = 0.0f, float y = 0.0f, float z = 0.0f):
		x(x),
		y(y),
		z(z)
		{
		}

		Vec3(const Vec2& vec, float z = 0.0f):
		x(vec.x),
		y(vec.y),
		z(z)
		{
		}

		Vec3 operator+(const Vec3& rhs) const
		{
			return Vec3(x + rhs.x, y + rhs.y, z + rhs.z);
		}

		Vec3 operator-(const Vec3& rhs) const
		{
			return Vec3(x - rhs.x, y - rhs.y, z - rhs.z);
		}

		Vec3 operator*(const float scalar) const
		{
			return Vec3(x*scalar, y*scalar, z*scalar);
		}

		Vec3 operator/(const float divider) const
		{
			return Vec3(x / divider, y / divider, z / divider);
		}

		Vec3& operator+=(const Vec3& rhs)
		{
			x += rhs.x;
			y += rhs.y;
			z += rhs.z;
			return *this;
		}

		Vec3& operator-=(const Vec3& rhs)
		{
			x -= rhs.x;
			y -= rhs.y;
			z -= rhs.z;
			return *this;
		}

		Vec3& operator*=(const float scalar)
		{
			x *= scalar;
			y *= scalar;
			z *= scalar;
			return *this;
		}

		Vec3& operator/=(const float divider)
		{
			x /= divider;
			y /= divider;
			z /= divider;
			return *this;
		}

		Vec3 operator-() const
		{
			return Vec3(*this).neg();
		}

		float getDotProduct(const Vec3& vec) const
		{
			return x * vec.x + y * vec.y + z * vec.z;
		}

		float getLength() const
		{
			return sqrtf(getDotProduct(*this));
		}

		Vec3& normalize()
		{

			if(const auto length = getLength(); length != 0.0f)
			{
				x /= length;
				y /= length;
				z /= length;
			}
			
			return *this;
		}

		Vec3 getNormalized() const
		{
			return Vec3(*this).normalize();
		}

		Vec3& interpolate(const float alpha, const Vec3& interpolateTo)
		{
			return *this = (interpolateTo - *this)*alpha + *this;
		}

		Vec3 getInterpolated(const float alpha, const Vec3& interpolateTo) const
		{
			return Vec3(*this).interpolate(alpha, interpolateTo);
		}

		Vec3& crossProduct(const Vec3& vec)
		{
			Vec3 tmp;
			tmp.x = y * vec.z - z * vec.y;
			tmp.y = z * vec.x - x * vec.z;
			tmp.z = x * vec.y - y * vec.x;
			return *this = tmp;
		}

		Vec3& getCrossProduct(const Vec3& vec) const
		{
			return Vec3(*this).crossProduct(vec);
		}

		Vec3& neg()
		{
			x = -x;
			y = -y;
			z = -z;
			return *this;
		}

		Vec2 xy() const
		{
			return Vec2(x, y);
		}

		union
		{
			float element[3]{};
			struct
			{
				float x;
				float y;
				float z;
			};
		};

	};

	__declspec(align(16))
	class Vec4
	{
	public:
		Vec4(float x = 0.0f, float y = 0.0f, float z = 0.0f, float w = 1.0f) :
			x(x),
			y(y),
			z(z),
			w(w)
		{
		}

		Vec4(const Vec3& vec, float w = 1.0f) :
			x(vec.x),
			y(vec.y),
			z(vec.z),
			w(w)
		{
		}

		Vec4(const Vec2& vec, float z = 0.0f, float w = 1.0f) :
			x(vec.x),
			y(vec.y),
			z(z),
			w(w)
		{
		}

		Vec4 operator+(const Vec4& rhs) const
		{
			return Vec4(x + rhs.x, y + rhs.y, z + rhs.z, w + rhs.w);
		}

		Vec4 operator-(const Vec4& rhs) const
		{
			return Vec4(x - rhs.x, y - rhs.y, z - rhs.z, w - rhs.w);
		}

		Vec4 operator*(const float scalar) const
		{
			return Vec4(x*scalar, y*scalar, z*scalar, w*scalar);
		}

		Vec4 operator/(const float divider) const
		{
			return Vec4(x / divider, y / divider, z / divider, w / divider);
		}

		Vec4& operator+=(const Vec4& rhs)
		{
			x += rhs.x;
			y += rhs.y;
			z += rhs.z;
			w += rhs.w;
			return *this;
		}

		Vec4& operator-=(const Vec4& rhs)
		{
			x -= rhs.x;
			y -= rhs.y;
			z -= rhs.z;
			w -= rhs.w;
			return *this;
		}

		Vec4& operator*=(const float scalar)
		{
			x *= scalar;
			y *= scalar;
			z *= scalar;
			w *= scalar;
			return *this;
		}

		Vec4& operator/=(const float divider)
		{
			x /= divider;
			y /= divider;
			z /= divider;
			w /= divider;
			return *this;
		}

		Vec4 operator-() const
		{
			return Vec4(*this).neg();
		}

		float getDotProduct(const Vec4& vec) const
		{
			return x * vec.x + y * vec.y + z * vec.z + w * vec.w;
		}

		float getLength() const
		{
			return sqrtf(getDotProduct(*this));
		}

		Vec4& normalize()
		{

			if (const auto length = getLength(); length != 0.0f)
			{
				x /= length;
				y /= length;
				z /= length;
				w /= length;
			}

			return *this;
		}

		Vec4 getNormalized() const
		{
			return Vec4(*this).normalize();
		}

		Vec4& interpolate(const float alpha, const Vec4& interpolateTo)
		{
			return *this = (interpolateTo - *this)*alpha + *this;
		}

		Vec4 getInterpolated(const float alpha, const Vec4& interpolateTo) const
		{
			return Vec4(*this).interpolate(alpha, interpolateTo);
		}

		Vec4& crossProduct(const Vec4& vec)
		{
			Vec4 tmp;
			tmp.x = y * vec.z - z * vec.y;
			tmp.y = z * vec.x - x * vec.z;
			tmp.z = x * vec.y - y * vec.x;
			return *this = tmp;
		}

		Vec4& getCrossProduct(const Vec4& vec) const
		{
			return Vec4(*this).crossProduct(vec);
		}

		Vec4& neg()
		{
			x = -x;
			y = -y;
			z = -z;
			w = -w;
			return *this;
		}
		Vec2 xy() const
		{
			return Vec2(x, y);
		}

		Vec3 xyz() const
		{
			return Vec3(x, y, z);
		}
		union
		{
			float element[4]{};
			struct
			{
				float x;
				float y;
				float z;
				float w;
			};
		};
	};

}

