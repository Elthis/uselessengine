#pragma once
#include "InputVertex.h"
template<class Vertex>
class VertexShader
{
	static_assert(std::is_base_of<InputVertex, Vertex>::value == 1, "Pipeline vertex must be derived from Vertex");
public:
	VertexShader() = default;
	
	virtual void process(Vertex& vertex) const = 0;
	virtual ~VertexShader() = default;
};
