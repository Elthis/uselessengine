#include "Window.h"
#include <utility>
#include "Game.h"
#include <iostream>
#include "Input.h"
//create window pointer to use it to access class function processing window events
Window* g_WINDOW;


Window::Window(std::string windowName, const size_t width, const size_t height): hwnd_(nullptr),
                                                                            width_(width),
                                                                            height_(height),
                                                                            windowName_(std::move(windowName))
{
	createWindow();
	ShowWindow(hwnd_, 1);
	UpdateWindow(hwnd_);
}

void Window::createWindow()
{
	g_WINDOW = this;
	WNDCLASSEX wc = {};
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = [](HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)-> LRESULT
	{
		return g_WINDOW->windowProcess(hwnd, msg, wParam, lParam);
	};
	wc.hInstance = GetModuleHandle(nullptr);
	wc.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
	wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wc.hbrBackground = reinterpret_cast<HBRUSH>(COLOR_WINDOW);
	wc.lpszClassName = windowName_.c_str();
	wc.hIconSm = LoadIcon(nullptr, IDI_APPLICATION);

	if (!RegisterClassEx(&wc))
		throw std::runtime_error("Failed to register window class");

	//Make client area equal width and height
	auto [wWidth, wHeight] = getWindowDimensions(width_, height_);

	if (!(hwnd_ = CreateWindowEx(0, windowName_.c_str(), windowName_.c_str(), WS_OVERLAPPEDWINDOW,
	                              CW_USEDEFAULT, CW_USEDEFAULT, wWidth, wHeight, nullptr, nullptr, GetModuleHandle(nullptr),
	                              nullptr)))
		throw std::runtime_error("Failed to create window");

}

std::tuple<size_t, size_t> Window::getWindowDimensions(size_t clientWidth, size_t clientHeight)
{
	RECT windowRect = { 0, 0, clientWidth, clientHeight };
	AdjustWindowRectEx(&windowRect, WS_OVERLAPPEDWINDOW, FALSE, NULL);
	return std::pair<size_t, size_t>(windowRect.right - windowRect.left, windowRect.bottom - windowRect.top);
}

LRESULT Window::windowProcess(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	
	switch (msg)
	{
	case WM_KEYDOWN:
		Input::Keyboard::pressKey(static_cast<char>(wParam));
		break;
	case WM_KEYUP:
		Input::Keyboard::releaseKey(static_cast<char>(wParam));
		break;
	case WM_LBUTTONDOWN:
		Input::Mouse::pressButton(Input::Mouse::LEFT);
		break;
	case WM_LBUTTONUP:
		Input::Mouse::releaseButton(Input::Mouse::LEFT);
		break;
	case WM_RBUTTONDOWN:
		Input::Mouse::pressButton(Input::Mouse::RIGHT);
		break;
	case WM_RBUTTONUP:
		Input::Mouse::releaseButton(Input::Mouse::RIGHT);
		break;
	case WM_MBUTTONDOWN:
		Input::Mouse::pressButton(Input::Mouse::MIDDLE);
		break;
	case WM_MBUTTONUP:
		Input::Mouse::releaseButton(Input::Mouse::MIDDLE);
		break;
	case WM_MOUSEMOVE:
		Input::Mouse::setPosition(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	

	default:
		return DefWindowProc(hwnd, msg, wParam, lParam);
	}
	return 0;
}


void Window::run()
{
	MSG msg;
	auto running = true;
	Game game(hwnd_, width_, height_);
	std::thread thread([&game, &running]
	{
		float dt = 0.0f;
		while (running)
		{
			auto start = std::chrono::high_resolution_clock::now();
			Input::Mouse::popMovement();
			game.loop(dt);
			auto finish = std::chrono::high_resolution_clock::now();
			dt = (finish - start).count()*0.00000001f;
		}
	});
	while (running) {
		while (GetMessage(&msg, nullptr, 0, 0))
		{

			TranslateMessage(&msg);
			DispatchMessage(&msg);		
		}
		running = false;
		thread.join();
	}
}
