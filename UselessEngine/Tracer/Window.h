#pragma once
#include <stdexcept>
#include <string>
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>


class Window
{
public:
	Window(std::string windowName, size_t width, size_t height);
	Window(const Window&) = delete;
	Window& operator=(const Window&) = delete;
	~Window() = default;
	void run();
	LRESULT windowProcess(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
private:
	void createWindow();
	static std::tuple<size_t, size_t> getWindowDimensions(size_t clientWidth, size_t clientHeight);

	HWND hwnd_;
	size_t width_;
	size_t height_;
	std::string windowName_;
};
